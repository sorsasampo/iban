import setuptools

setuptools.setup(
    name='iban',
    version='0.0.1',
    author='Sampo Sorsa',
    author_email='sorsasampo@protonmail.com',
    packages=setuptools.find_packages(),
    classifiers=[
        'License :: OSI Approved :: Apache Software License',
        'Operating System :: POSIX :: Linux',
        'Programming Language :: Python :: 3',
    ],
    entry_points={
        'console_scripts': [
            'iban=iban:main',
        ],
    },
    install_requires=[
        'schwifty',
    ],
    python_requires='>=3.5',
)
