Command line tool to validate IBANs.

# Install

    $ python3 setup.py install --user

# Usage

    $ iban validate "BE71 0961 2345 6769" "FR76 3000 6000 0112 3456 7890 189" invalid
    BE71 0961 2345 6769     OK
    FR76 3000 6000 0112 3456 7890 189       OK
    invalid INVALID

The output is tab-separated, so you can use `column` to columnate it:

    $ iban validate "BE71 0961 2345 6769" "FR76 3000 6000 0112 3456 7890 189" invalid |column -ts $'\t'
    BE71 0961 2345 6769                OK
    FR76 3000 6000 0112 3456 7890 189  OK
    invalid                            INVALID

# Dependencies

* [schwifty](https://pypi.org/project/schwifty/) is currently used for validation.
