#!/usr/bin/env python3
# Copyright (c) 2018 Sampo Sorsa <sorsasampo@protonmail.com>
import argparse
import re
import sys

import schwifty

IBAN_REGEX = '\\b[A-Z]{2}[0-9]{2}(?:\\s*[0-9A-Z]{4}){,7}(?:\\s*[0-9A-Z]{0,3})\\b'


def command_validate(args):
    ibans = args.iban
    if args.fromfile is not None:
        if args.search:
            data = args.fromfile.read()
            ibans = re.findall(IBAN_REGEX, data)
            ibans = [''.join(i.split()) for i in ibans]
        else:
            ibans = map(lambda line: line.rstrip('\n'), args.fromfile)
    for iban in ibans:
        if is_valid(iban):
            i = schwifty.IBAN(iban)
            if args.formatted:
                iban = i.formatted
            else:
                iban = i.compact
        print('%s\t%s' % (iban, 'OK' if is_valid(iban) else 'INVALID'))


def is_valid(iban):
    try:
        schwifty.IBAN(iban)
    except ValueError:
        return False
    return True


def main(argv=sys.argv[1:]):
    args = parser.parse_args(argv)
    args.func(args)


parser = argparse.ArgumentParser(
    description='IBAN validator',
)
subparsers = parser.add_subparsers()
parser_validate = subparsers.add_parser('validate')
parser_validate.add_argument(
    '--compact',
    action='store_false',
    dest='formatted',
)
parser_validate.add_argument(
    '--formatted',
    default=True,
    action='store_true',
    dest='formatted',
)
parser_validate.add_argument(
    '--from',
    type=argparse.FileType('r'),
    dest='fromfile',
)
parser_validate.add_argument(
    '--search',
    action='store_true',
    help='Find all IBANs from the given file, even if in the middle of lines or wrapped over multiple lines.',
)
parser_validate.add_argument(
    'iban',
    nargs='*',
)
parser_validate.set_defaults(func=command_validate)

if __name__ == '__main__':
    main()
