import contextlib
import io
import unittest
import unittest.mock

import iban

# From https://en.wikipedia.org/wiki/IBAN#Structure
VALID_IBANS = [
    'BE71 0961 2345 6769',
    'FR76 3000 6000 0112 3456 7890 189',
    'DE91 1000 0000 0123 4567 89',
    'GR96 0810 0010 0000 0123 4567 890',
    'RO09 BCYP 0000 0012 3456 7890',
    'SA44 2000 0001 2345 6789 1234',
    'ES79 2100 0813 6101 2345 6789',
    'CH56 0483 5012 3456 7800 9',
    'GB98 MIDL 0700 9312 3456 78',
]
VALID_IBANS_OUTPUT = '\n'.join(['%s\tOK' % i for i in VALID_IBANS]) + '\n'


class TestIban(unittest.TestCase):
    def setUp(self):
        self.maxDiff = None

    def test_iban_regex(self):
        for i in VALID_IBANS:
            self.assertRegex(i, iban.IBAN_REGEX)

    def test_validate_compact(self):
        f = io.StringIO()
        with contextlib.redirect_stdout(f):
            iban.main(['validate', '--compact', 'BE71 0961 2345 6769'])
        self.assertEqual(f.getvalue(), 'BE71096123456769\tOK\n')

    def test_validate_from(self):
        f = io.StringIO()
        stdin = io.StringIO('\n'.join(VALID_IBANS))
        with unittest.mock.patch('sys.stdin', stdin):
            with contextlib.redirect_stdout(f):
                iban.main(['validate', '--from', '-'])
        self.assertEqual(f.getvalue(), VALID_IBANS_OUTPUT)

    def test_validate_invalid(self):
        f = io.StringIO()
        with contextlib.redirect_stdout(f):
            iban.main(['validate', 'invalid'])
        self.assertEqual(f.getvalue(), 'invalid\tINVALID\n')

    def test_validate_search(self):
        f = io.StringIO()
        stdin = io.StringIO(
            """
            IBAN account numbers can be even read from a file. For example FR76
            3000 6000 0112 3456 7890 189 and GR96 0810 0010 0000 0123 4567 890
            SHOULD be valid IBANs, even if they are wrapped.

            GB98 MIDL 0700 9312 3456 78 as well.
            """)
        with unittest.mock.patch('sys.stdin', stdin):
            with contextlib.redirect_stdout(f):
                iban.main(['validate', '--search', '--from', '-'])
        self.assertEqual(
            f.getvalue(),
            'FR76 3000 6000 0112 3456 7890 189\tOK\n'
            'GR96 0810 0010 0000 0123 4567 890\tOK\n'
            'GB98 MIDL 0700 9312 3456 78\tOK\n'
        )

    def test_validate_valid(self):
        f = io.StringIO()
        with contextlib.redirect_stdout(f):
            iban.main(['validate'] + VALID_IBANS)
        self.assertEqual(f.getvalue(), VALID_IBANS_OUTPUT)


if __name__ == '__main__':
    unittest.main(buffer=True)
